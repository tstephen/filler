/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 10:44:50 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/17 12:31:17 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H
# include "./libft/libft.h"
# include <fcntl.h>
# include <mlx.h>
# define LARGE_DISTANCE 100000000
# define SIZE 10

typedef struct		s_player
{
	char			**token;
	char			**cut_token;
	int				row_cut_token;
	int				col_cut_token;
	int				row_token;
	int				col_token;
	int				offset[4];
	char			user_symbol;
	char			opp_symbol;
}					t_player;

typedef struct		s_map
{
	char			**map;
	int				row_map;
	int				col_map;
}					t_map;

typedef struct		s_moves
{
	int				co_ord[2];
	int				distance;
	struct s_moves	*next;
}					t_moves;

typedef struct		s_mlx
{
	void			*mlx;
	void			*window;
	void			*image;
	char			*data;
	int				bpp;
	int				size_line;
	int				endian;
	int				window_size[2];
	int				dimensions[2];
	int				enemy_color[3];
	int				my_color[3];
	int				inside[3];
	int				outline[3];
	t_map			*map;
	t_player		*player;
	int				fd;
}					t_mlx;

/*------GETTING_INPUT-----------*/
void				ft_get_map(t_map *map, int fd);
int					ft_nbrlen(int n);
void				ft_get_token_info(t_player *player, int fd);
void				ft_store_offset(t_player *player);
void				ft_cut_token(t_player *player);
void				ft_get_map_dimensions(t_map *map, char *line);

/*------DISPLAY_STUFF-----------*/
void				ft_print_map(t_map *map);
void				ft_print_token(t_player *player);
void				ft_print_offset(t_player *player);
void				ft_print_cut_token(t_player *player);
void				ft_print_validity(t_player *player, t_map *map, int move[]);
void				ft_print_valid_moves_list(t_moves *moves);
void				ft_print_dist_moves_list(t_moves *moves);

/*-----------UTILITY------------*/
int					ft_my_territory(t_player *player, t_map *map, int i, int j);
int					ft_enemy_territory(t_player *player, t_map *map, int i, int j);
void				ft_free_game_info(t_player *player, t_map *map);

/*--------VERIFY_MOVE-----------*/
int					ft_valid_placement(t_player *player, t_map *map, int move[]);

/*--------MOVES_LIST------------*/
t_moves				*ft_valid_moves(t_player *player, t_map *map);
void				ft_moves_push_back(t_moves **head, t_moves *move);
t_moves				*ft_new_move(int x, int y);
int					ft_moves_list_size(t_moves *moves);
void				ft_moves_list_del(t_moves **head);

/*---------DISTANCE-------------*/
t_moves				*ft_find_distances(t_player *player, t_map *map);
int					ft_abs(int n);
int					ft_shortest_distance(t_player *player, t_map *map, int move[]);
int					ft_total_distance(t_player *player, t_map *map, t_moves *move);

/*----------PLAY----------------*/
void				ft_push_co_ord(t_player *player, t_map *map);
void				ft_print_co_ord(int i, int j);


/*---------GRAPHICS-------------*/
int					game_loop(t_mlx *mlx);
void				ft_draw_rect(t_mlx *mlx, int start[], int dimensions[], char type);
void				mlx_image_pixel_put(t_mlx *mlx, int x, int y, int color[]);
t_mlx				*ft_setup_mlx();
void				ft_draw_basic_grid(t_mlx *mlx);
void				ft_fill_territory(t_player *player, t_map *map, t_mlx *mlx);
void				ft_set_colors(t_mlx *mlx);

#endif
