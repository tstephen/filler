/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utility.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 12:43:39 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/15 13:34:07 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			ft_nbrlen(int n)
{
	int		counter;

	counter = 0;
	while (n != 0)
	{
		n /= 10;
		counter++;
	}
	return (counter);
}

void		ft_free_game_info(t_player *player, t_map *map)
{
	int		i;

	i = 0;
	while (i < player->row_cut_token)
	{
		free(player->cut_token[i]);
		i++;
	}
	i = 0;
	while (i < player->row_token)
	{
		free(player->token[i]);
		i++;
	}
	i = 0;
	while (i < map->row_map)
	{
		free(map->map[i]);
		i++;
	}
}

int			ft_my_territory(t_player *player, t_map *map, int i, int j)
{
	if (map->map[i][j] == player->user_symbol ||
		map->map[i][j] == ft_tolower(player->user_symbol))
		return (1);
	return (0);
}

int			ft_enemy_territory(t_player *player, t_map *map, int i, int j)
{
	if (map->map[i][j] == player->opp_symbol ||
		map->map[i][j] == ft_tolower(player->opp_symbol))
		return (1);
	return (0);
}

void		ft_print_map(t_map *map)
{
	int		i;

	i = 0;
	while (i < map->row_map)
	{
		ft_putendl(map->map[i]);
		i++;
	}
}

void		ft_print_token(t_player *player)
{
	int		i;

	i = 0;
	while (i < player->row_token)
	{
		ft_putendl(player->token[i]);
		i++;
	}
}

void		ft_print_offset(t_player *player)
{
	ft_putstr("offset->top : ");
	ft_putnbr(player->offset[0]);
	ft_putchar('\n');
	ft_putstr("offset->right : ");
	ft_putnbr(player->offset[1]);
	ft_putchar('\n');
	ft_putstr("offset->bot : ");
	ft_putnbr(player->offset[2]);
	ft_putchar('\n');
	ft_putstr("offset->left : ");
	ft_putnbr(player->offset[3]);
	ft_putchar('\n');
}

void		ft_print_cut_token(t_player *player)
{
	int	i;

	i = 0;
	while (i < player->row_cut_token)
	{
		ft_putendl(player->cut_token[i]);
		i++;
	}
}

void		ft_print_validity(t_player *player, t_map *map, int move[])
{
	ft_putstr("CO_ORD ");
	ft_putnbr(move[0]);
	ft_putchar(' ');
	ft_putnbr(move[1]);
	ft_putstr(":  ");
	ft_putnbr(ft_valid_placement(player, map, move));
	ft_putchar('\n');
}

void		ft_print_valid_moves_list(t_moves *moves)
{
	while (moves)
	{
		ft_putnbr(moves->co_ord[0]);
		ft_putchar(' ');
		ft_putnbr(moves->co_ord[1]);
		ft_putchar('\n');
		moves = moves->next;
	}
}

int		ft_moves_list_size(t_moves *moves)
{
	int	i;

	i = 0;
	while (moves)
	{
		i++;
		moves = moves->next;
	}
	return (i);
}

void	ft_moves_list_del(t_moves **head)
{
	t_moves *next;

	while (*head)
	{
		next = (*head)->next;
		free(*head);
		*head = next;
	}
	*head = NULL;
}

int		ft_abs(int n)
{
	return (n < 0 ? -1 * n : n);
}

void	ft_print_dist_moves_list(t_moves *moves)
{
	while (moves)
	{
		ft_putnbr(moves->co_ord[0]);
		ft_putchar(' ');
		ft_putnbr(moves->co_ord[1]);
		ft_putstr("  : DIST -- ");
		ft_putnbr(moves->distance);
		ft_putchar('\n');
		moves = moves->next;
	}
}
