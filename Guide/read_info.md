# GETTING INPUT FROM THE VM
## PLAYER SYMBOL

* The players symbols are either defined by an *'O'* for player *1* and an *'X'* for player *2*. This will be sent to our program via the game manager and we can read this initial line and look for what player number we are. This will always be the first line sent by the game manager. Only the respective player number is sent to the related player.

---
## MAP

* The first line sent to us will be the description of the map in terms of rows and columns. We can therefore determine the dimensions of the map.
* Once we know the map dimensions we can get the rest of the map by looping that many times.

---
## TOKEN

* The token dimensions are then sent to us and we can get grab the token in a similar way as to how we grabbed the map.