# THE BRAINS

## CO-ORDINATE ARRAY

* We can obtain and push the first co-ordinate of our territory into co-ordinate array.
* Subsequents tokens placements will increase the array by the number of '*' - 1. We then push the new co-ordinates to the array.

---
## SCORE

* The score will be calculated based on the distance between a co-ord in my territory and the closest enemy co-ord.
* The score will also be affected by the availability of the co-ord.
* The lower the score the better it is.

---
## SORT BY SCORE

* We can sort the co-ords to be by lowest score to highest.

---
## ATTEMPT TO PLACE AT POSITION

* We can now place the token at its position based on its score.

---