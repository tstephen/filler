<!-- Headings --->
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

<!-- Italics -->
*This text* is italic

_This text_ is italic

<!-- Strong -->

**This text** is strong

__This text__ is strong

<!-- Strikethrough -->
~~This text~~ is strikethrough

<!-- Horizontal Rule-->

---

___

We can escape with a \

<!-- Block quotes -->
> This is a quote

<!-- Links -->
[MY LINK](http://www.google.com)

[MY LINK](http://www.google.com "Some search engine")

<!-- UL -->
* Item 1
* Item 2
* Item 3
	* Nested Item 1
	* Nested Item 2

<!-- OL -->
1. Item 1
2. Item 2
3. Item 3

<!-- Inline code block -->
`<p>This is a paragraph</p>`

<!-- Images -->
![Markdown Logo](https:/markdown-here.com/img/icon256.png)

<!-- Github Markdown -->

<!-- Code block -->
```
	npm install

	npm start
```

```javascript
	function add(num1, num2){
		return (num1 + num2);
	}
```

```python
	def add(num1, num2):
		return (num1 + num2)
```

<!-- Tables -->
| Name             | Email            |
| -----------------| ---------------- |
| Tyrone  | tlstephen11@gmail.com |


<!-- Tasks Lists -->
* [x] Task 1
* [x] Task 2
* [ ] Task 3