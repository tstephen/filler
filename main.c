/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 13:23:03 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/17 12:27:06 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_play(t_player *player, t_map *map, int fd)
{
	ft_get_map(map, fd);
	ft_get_token_info(player, fd);
	ft_store_offset(player);
	ft_cut_token(player);
	ft_push_co_ord(player, map);
}

void		ft_start_mlx(t_map *map, t_mlx *mlx)
{
	mlx->dimensions[0] = 10;
	mlx->dimensions[1] = 10;
	mlx->window_size[0] = map->col_map * mlx->dimensions[0];
	mlx->window_size[1] = map->row_map * mlx->dimensions[1];
	mlx->window = mlx_new_window(mlx->mlx, mlx->window_size[0], mlx->window_size[1], "filler");
	mlx->image = mlx_new_image(mlx->mlx, mlx->window_size[0], mlx->window_size[1]);
	mlx->data = mlx_get_data_addr(mlx->image, &(mlx->bpp), &(mlx->size_line), &(mlx->endian));
	mlx->bpp /= 8;
}

int			game_loop(t_mlx *mlx)
{
	static	int			i = 0;
	char				*line;

	if (mlx->fd == -1)
		return (0);
	if (get_next_line(mlx->fd, &line) == 0)
	{
		free(line);
		ft_free_game_info(mlx->player, mlx->map);
		mlx_destroy_image(mlx->mlx, mlx->image);
		mlx_destroy_window(mlx->mlx, mlx->window);
		mlx->fd = -1;
		exit (1);
	}
	ft_get_map_dimensions(mlx->map, line);
	free(line);
	ft_play(mlx->player, mlx->map, mlx->fd);
	if (i++ == 0)
	{
		ft_start_mlx(mlx->map, mlx);
		ft_draw_basic_grid(mlx);
	}
	ft_fill_territory(mlx->player, mlx->map, mlx);
	mlx_put_image_to_window(mlx->mlx, mlx->window, mlx->image, 0, 0);
	return (1);
}

int			main(void)
{
	t_mlx		*mlx;
	t_player	*player;
	t_map		*map;
	char		*line;
	
	map = (t_map*)ft_memalloc(sizeof(t_map));
	player = (t_player*)ft_memalloc(sizeof(t_player));
	mlx = ft_setup_mlx();
//	mlx->fd = open("Sample_Maps/test_co_ords.txt", O_RDONLY);
	mlx->fd = 0;
	mlx->player = player;
	mlx->map = map;
	ft_set_colors(mlx);
	get_next_line(mlx->fd, &line);
	mlx->player->user_symbol = ft_atoi(line + 10) == 1 ? 'O' : 'X';
	mlx->player->opp_symbol = mlx->player->user_symbol == 'X' ? 'O' : 'X';
	free(line);
	mlx_loop_hook(mlx->mlx, game_loop, mlx);
	if (mlx->fd == -1)
		return (0);
	mlx_loop(mlx->mlx);
	return (0);
}
