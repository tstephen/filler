/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   distance.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 12:33:13 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/14 14:47:58 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			*ft_find_next_enemy(t_player *player, t_map *map, int start[])
{
	while (start[0] < map->row_map)
	{
		while (start[1] < map->col_map)
		{
			if (ft_enemy_territory(player, map, start[0], start[1]))
				return (start);
			start[1]++;
		}
		start[1] = 0;
		start[0]++;
	}
	start[0] = LARGE_DISTANCE;
	start[1] = LARGE_DISTANCE;
	return (start);
}

int			ft_shortest_distance(t_player *player, t_map *map, int move[])
{
	int		distance;
	int		start[2];
	int		temp;

	start[0] = -1;
	start[1] = -1;
	distance = LARGE_DISTANCE;
	while (start[0] != LARGE_DISTANCE && start[1] != LARGE_DISTANCE)
	{
		if (start[0] == -1 || start[1] == -1)
		{
			start[0] = 0;
			start[1] = 0;
		}
		else
		{
			start[0]++;
			start[1]++;
		}
		ft_find_next_enemy(player, map, start);
		temp = ft_abs(move[0] - start[0]) + ft_abs(move[1] - start[1]);
		if (temp < distance)
			distance = temp;
	}
	return (distance);
}

int			ft_total_distance(t_player *player, t_map *map, t_moves *move)
{
	int		token_co_ord[2];
	int		map_co_ord[2];
	int		distance;

	distance = 0;
	token_co_ord[0] = 0;
	token_co_ord[0] = 0;
	while (token_co_ord[0] < player->row_cut_token)
	{
		while (token_co_ord[1] < player->col_cut_token)
		{
			map_co_ord[0] = move->co_ord[0];
			map_co_ord[1] = move->co_ord[1];
			if(player->cut_token[token_co_ord[0]][token_co_ord[1]] == '*')
			{
				map_co_ord[0] += token_co_ord[0];
				map_co_ord[1] += token_co_ord[1];
				distance += ft_shortest_distance(player, map, map_co_ord);
			}
			token_co_ord[1]++;
		}
		token_co_ord[0]++;
		token_co_ord[1] = 0;
	}
	return (distance);
}

t_moves		*ft_find_distances(t_player *player, t_map *map)
{
	t_moves		*available_moves;
	t_moves		*current;

	available_moves = ft_valid_moves(player, map);
	current = available_moves;
	while (current)
	{
		current->distance = ft_total_distance(player, map, current);
		current = current->next;
	}
	return (available_moves);
}
