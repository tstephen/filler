/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 12:41:51 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/15 13:34:03 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void			ft_get_token_info(t_player *player, int fd)
{
	char	*line;
	int		row;

	get_next_line(fd, &line);
	player->row_token = ft_atoi(&line[6]);
	player->col_token = ft_atoi(&line[6 + ft_nbrlen(player->row_token)]);
	free(line);
	row = 0;
	player->token = (char**)ft_memalloc(sizeof(char**) * player->row_token);
	while (row < player->row_token)
	{
		get_next_line(fd, &((player->token)[row]));
		row++;
	}
}

static	void	ft_get_left_top_offset(t_player *player)
{
	int		i;
	int		j;
	int		min_i;
	int		min_j;

	i = 0;
	j = 0;
	min_i = player->row_token;
	min_j = player->col_token;
	while (i < player->row_token)
	{
		while (j < player->col_token)
		{
			if (player->token[i][j] == '*')
			{
				min_i = i < min_i ? i : min_i;
				min_j = j < min_j ? j : min_j;
			}
			j++;
		}
		j = 0;
		i++;
	}
	player->offset[0] = min_i;
	player->offset[3] = min_j;
}

static	void	ft_get_right_bot_offset(t_player *player)
{
	int		i;
	int		j;
	int		max_j;
	int		max_i;

	i = player->row_token - 1;
	j = player->col_token - 1;
	max_j = 0;
	max_i = 0;
	while (i >= 0)
	{
		while (j >= 0)
		{
			if (player->token[i][j] == '*')
			{
				max_i = i > max_i ? i : max_i;
				max_j = j > max_j ? j : max_j;
			}
			j--;
		}
		j = player->col_token;
		i--;
	}
	player->offset[1] = player->col_token - 1 - max_j;
	player->offset[2] = player->row_token - 1 - max_i;
}

void			ft_cut_token(t_player *player)
{
	int		counter;
	int		start;

	counter = 0;
	player->row_cut_token = player->row_token - player->offset[0] -
		player->offset[2];
	player->col_cut_token = player->col_token - player->offset[1] -
		player->offset[3];
	start = player->offset[0];
	player->cut_token = (char**)ft_memalloc(sizeof(char*) * player->
			row_cut_token);
	while (counter < player->row_cut_token)
	{
		player->cut_token[counter] = ft_strsub(player->token[start],
				player->offset[3], player->col_cut_token);
		counter++;
		start++;
	}
}

void			ft_store_offset(t_player *player)
{
	ft_get_left_top_offset(player);
	ft_get_right_bot_offset(player);
	ft_cut_token(player);
}
