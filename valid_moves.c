/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_moves.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/03 13:46:21 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/15 13:34:00 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_valid_placement(t_player *player, t_map *map, int move[])
{
	int		row;
	int		col;
	int		overlap;
	int		i;
	int		j;

	overlap = 0;
	row = move[0];
	col = move[1];
	i = 0;
	j = 0;
	while (i < player->row_cut_token)
	{
		while (j < player->col_cut_token)
		{
			if (row > map->row_map - player->row_cut_token + i || col > map->col_map - player->col_cut_token + j)
				return (0);
			if (ft_my_territory(player, map, row, col) && player->cut_token[i][j] == '*')
				overlap++;
			if (ft_enemy_territory(player, map, row, col) && player->cut_token[i][j] == '*')
				return (0);
			j++;
			col++;
			if (overlap > 1)
				return (0);
		}
		row++;
		col = move[1];
		j = 0;
		i++;
	}
	return (overlap == 1 ? 1 : 0);
}

t_moves		*ft_new_move(int x, int y)
{
	t_moves		*new;

	new = (t_moves*)ft_memalloc(sizeof(t_moves));
	new->co_ord[0] = x;
	new->co_ord[1] = y;
	new->next = NULL;
	return (new);
}

void		ft_moves_push_back(t_moves **head, t_moves *move)
{
	t_moves		*current;

	current = *head;
	if (!*head)
		*head = move;
	else
	{
		while (current->next)
			current = current->next;
		current->next = move;
	}
}

t_moves		*ft_valid_moves(t_player *player, t_map *map)
{
	t_moves *head;
	t_moves	*new_move;
	int		move[2];

	move[0] = 0;
	move[1] = 0;
	head = NULL;
	while (move[0] < map->row_map)
	{
		while (move[1] < map->col_map)
		{
			if (ft_valid_placement(player, map, move))
			{
				new_move = ft_new_move(move[0], move[1]);
				ft_moves_push_back(&head, new_move); 
			}
			move[1]++;
		}
		move[1] = 0;
		move[0]++;
	}
	return (head);
}
