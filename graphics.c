/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graphics.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/12 13:41:42 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/17 12:29:28 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

t_mlx	*ft_setup_mlx()
{
	t_mlx	*mlx;

	mlx = (t_mlx*)ft_memalloc(sizeof(t_mlx));
	mlx->mlx = mlx_init();
	return (mlx);
}

void	ft_draw_basic_grid(t_mlx *mlx)
{
	int	start[2];

	start[0] = 0;
	start[1] = 0;
	while (start[1] < mlx->window_size[1])
	{
		while (start[0] < mlx->window_size[0])
		{
			ft_draw_rect(mlx, start, mlx->dimensions, 'N');
			start[0] += mlx->dimensions[0];
		}
		start[0] = 0;
		start[1] += mlx->dimensions[1];
	}
}

void		mlx_image_pixel_put(t_mlx *mlx, int x, int y, int color[])
{
	mlx->data[(y * mlx->size_line) + (x * (mlx->bpp))] = 
					mlx_get_color_value(mlx->mlx, color[2]);
	mlx->data[(y * mlx->size_line) + (x * (mlx->bpp)) + 1] = 
					mlx_get_color_value(mlx->mlx, color[1]);
	mlx->data[(y * mlx->size_line) + (x * (mlx->bpp)) + 2] = 
					mlx_get_color_value(mlx->mlx, color[0]);
}

void	ft_draw_rect(t_mlx *mlx, int start[], int dimensions[], char type)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (i < dimensions[1])
	{
		while (j < dimensions[0])
		{
			if (i == 0 || j == 0 || i == dimensions[1] - 1 || j == dimensions[0] - 1)
				mlx_image_pixel_put(mlx, start[0] + j, start[1] + i, mlx->outline);
			else if (type == 'N')
				mlx_image_pixel_put(mlx, start[0] + j, start[1] + i, mlx->inside);
			else if (type == 'A')
				mlx_image_pixel_put(mlx, start[0] + j, start[1] + i, mlx->my_color);
			else if (type == 'E')
				mlx_image_pixel_put(mlx, start[0] + j, start[1] + i, mlx->enemy_color);
			j++;
		}
		j = 0;
		i++;
	}
}

void		ft_fill_territory(t_player *player, t_map *map, t_mlx *mlx)
{
	int	start[2];
	int	i;
	int	j;

	i = 0;
	j = 0;
	start[0] = 0;
	start[1] = 0;
	while (i < map->row_map)
	{
		while (j < map->col_map)
		{
			if (ft_my_territory(player, map, i, j))
				ft_draw_rect(mlx, start, mlx->dimensions, 'A');
			else if (ft_enemy_territory(player, map, i, j))
				ft_draw_rect(mlx, start, mlx->dimensions, 'E');
			start[0] = (j + 1) * mlx->dimensions[0];
			j++;
		}
		start[0] = 0;
		start[1] = (i + 1) * mlx->dimensions[1];
		j = 0;
		i++;
	}
}

void		ft_set_colors(t_mlx *mlx)
{
	mlx->enemy_color[0] = 150;
	mlx->enemy_color[1] = 20;
	mlx->enemy_color[2] = 30;
	mlx->my_color[0] = 10;
	mlx->my_color[1] = 50;
	mlx->my_color[2] = 130;
	mlx->inside[0] = 150;
	mlx->inside[1] = 150;
	mlx->inside[2] = 150;
	mlx->outline[0] = 20;
	mlx->outline[1] = 20;
	mlx->outline[1] = 20;
}
