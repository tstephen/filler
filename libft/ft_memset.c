/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 12:09:38 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 08:26:48 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	chr;
	unsigned char	*ptr;
	size_t			i;

	if (!b)
		return (NULL);
	chr = (unsigned char)c;
	ptr = (unsigned char*)b;
	i = 0;
	while (i < len)
	{
		*ptr = chr;
		ptr++;
		i++;
	}
	return (b);
}
