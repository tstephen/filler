/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/23 19:52:30 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/03 14:54:11 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_find_length(int n)
{
	int counter;

	counter = 0;
	if (n == 0)
	{
		return (counter + 1);
	}
	while (n != 0)
	{
		counter++;
		n /= 10;
	}
	return (counter);
}

static char	*ft_build_arr(int n, int sign, int length)
{
	int		counter;
	char	*result;

	counter = length - 1;
	if (n == 0)
	{
		if (!(result = ft_strnew(1)))
			return (NULL);
		*result = '0';
		return (result);
	}
	if (!(result = (char*)malloc(sizeof(*result) * length + 1)))
		return (NULL);
	if (sign == -1)
		result[0] = '-';
	while (n != 0)
	{
		result[counter] = n % 10 + '0';
		counter--;
		n /= 10;
	}
	return (result);
}

char		*ft_itoa(int n)
{
	int		sign;
	int		length;
	char	*result;

	if (n == INT_MIN)
		return (!(result = ft_strdup("-2147483648")) ? NULL : result);
	sign = 1;
	if (n < 0)
	{
		n *= -1;
		sign = -1;
	}
	length = sign == -1 ? ft_find_length(n) + 1 : ft_find_length(n);
	if (!(result = ft_build_arr(n, sign, length)))
		return (NULL);
	result[length] = '\0';
	return (result);
}
