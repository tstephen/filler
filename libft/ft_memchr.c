/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 15:42:49 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 08:31:07 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned	char	*ptr_s;
	unsigned	char	chr;
	size_t				i;

	i = 0;
	if (!s)
		return (NULL);
	chr = (unsigned char)c;
	ptr_s = (unsigned char*)s;
	while (i < n)
	{
		if (*ptr_s == chr)
			return (ptr_s);
		i++;
		ptr_s++;
	}
	return (NULL);
}
