/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 21:58:49 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 12:07:42 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s1, size_t n)
{
	size_t	i;
	char	*result;
	size_t	len;

	i = 0;
	if (!s1)
		return (NULL);
	len = ft_strlen(s1) > n ? n : ft_strlen(s1);
	if (!(result = ft_strnew(len)))
		return (NULL);
	while (s1[i] && i < n)
	{
		result[i] = s1[i];
		i++;
	}
	return (result);
}
