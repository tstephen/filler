/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_split.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/05 09:06:45 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/09 10:48:54 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lst_split(char *s, char c)
{
	t_list	*head;
	t_list	*current;
	char	**res;
	int		i;

	i = 0;
	head = NULL;
	if (!(res = ft_strsplit(s, c)))
		return (NULL);
	while (res[i])
	{
		current = ft_lstnew((void*)(res[i]), ft_strlen(res[i]) + 1);
		ft_lst_push_back(&head, current);
		i++;
	}
	ft_arr_del((void**)res);
	return (head);
}
