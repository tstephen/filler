/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 13:03:58 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 08:28:21 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned	char	*ptr_dest;
	unsigned	char	*ptr_src;
	size_t				i;

	i = 0;
	if (!dst || !src)
		return (NULL);
	ptr_dest = (unsigned char*)dst;
	ptr_src = (unsigned char*)src;
	while (i < n)
	{
		*ptr_dest = *ptr_src;
		ptr_dest++;
		ptr_src++;
		i++;
	}
	return (dst);
}
