/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_pack.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/06 09:05:34 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/06 09:33:51 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lst_push_back(t_list **head, t_list *new)
{
	t_list	*current;

	current = *head;
	if (!(current))
	{
		*head = new;
	}
	else
	{
		while (current->next)
			current = current->next;
		current->next = new;
	}
}