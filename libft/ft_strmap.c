/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 09:19:20 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:26:20 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int			i;
	int			counter;
	char		*result;

	i = 0;
	counter = 0;
	if (!s)
		return (NULL);
	while (s[i])
		i++;
	if (!(result = ft_strnew(i)))
		return (NULL);
	while (counter < i)
	{
		result[counter] = (*f)(s[counter]);
		counter++;
	}
	return (result);
}
