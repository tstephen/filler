/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/24 20:44:49 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 08:29:34 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char		*ptr_src;
	unsigned char		*ptr_dst;
	size_t				i;

	i = 0;
	if (!dst || !src)
		return (NULL);
	ptr_dst = (unsigned char*)dst;
	ptr_src = (unsigned char*)src;
	if (ptr_src >= ptr_dst)
		while (i < len)
		{
			*ptr_dst = *ptr_src;
			ptr_dst++;
			ptr_src++;
			i++;
		}
	else
		while (i < len--)
		{
			ptr_dst[len] = ptr_src[len];
		}
	return (dst);
}
