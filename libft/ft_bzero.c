/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 12:56:48 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 12:05:50 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t n)
{
	size_t				i;
	unsigned	char	*ptr;

	if (!s)
		return ;
	i = 0;
	ptr = (unsigned char*)s;
	while (i < n)
	{
		*ptr = 0;
		ptr++;
		i++;
	}
}
