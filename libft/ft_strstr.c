/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 11:18:40 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 10:16:12 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_check_full_substr(const char *str, const char *substr, int i)
{
	while (*substr && str[i])
	{
		if (str[i] != *substr)
		{
			return (0);
		}
		i++;
		substr++;
	}
	if (!str[i] && *substr)
	{
		return (0);
	}
	return (1);
}

char			*ft_strstr(const char *haystack, const char *needle)
{
	int		i;
	int		check;
	char	*found;

	if (!haystack || !needle)
		return (NULL);
	i = 0;
	check = 0;
	if (!(*needle))
		return ((char*)haystack);
	while (haystack[i])
	{
		if (haystack[i] == needle[0])
		{
			check = ft_check_full_substr(haystack, needle, i);
			if (check == 1)
			{
				found = (char*)(haystack + i);
				return (found);
			}
		}
		i++;
	}
	return (NULL);
}
