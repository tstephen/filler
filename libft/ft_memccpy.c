/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 13:33:57 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 08:28:59 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned	char	*ptr_dst;
	unsigned	char	*ptr_src;
	size_t				i;
	unsigned	char	chr;

	i = 0;
	if (!dst || !src)
		return (NULL);
	chr = (unsigned char)c;
	ptr_dst = (unsigned char*)dst;
	ptr_src = (unsigned char*)src;
	while (i < n)
	{
		*ptr_dst = *ptr_src;
		i++;
		ptr_dst++;
		if (*ptr_src == chr)
			return (ptr_dst);
		ptr_src++;
	}
	return (NULL);
}
