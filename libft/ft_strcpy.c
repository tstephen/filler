/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/12 17:21:55 by tstephen          #+#    #+#             */
/*   Updated: 2018/06/04 08:34:45 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dest, const char *src)
{
	int	counter;

	if (!dest || !src)
		return (NULL);
	counter = 0;
	while (*src)
	{
		dest[counter] = *src;
		counter++;
		src++;
	}
	dest[counter] = '\0';
	return (dest);
}
