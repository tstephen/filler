/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 13:45:27 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/15 13:34:01 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_print_co_ord(int i, int j)
{
	ft_putnbr(i);
	ft_putchar(' ');
	ft_putnbr(j);
	ft_putchar('\n');
}

void		ft_push_co_ord(t_player *player, t_map *map)
{
	t_moves		*moves;
	int			max;
	int			co_ord[2];
	t_moves		*head;

	max = LARGE_DISTANCE;
	co_ord[0] = 0;
	co_ord[1] = 0;
	moves = ft_find_distances(player, map);
	head = moves;
	if (ft_moves_list_size(moves) != 0)
	{
		while (moves)
		{
			if (moves->distance < max)
			{
				max = moves->distance;
				co_ord[0] = moves->co_ord[0];
				co_ord[1] = moves->co_ord[1];
			}
			moves = moves->next;
		}
	}
	if (ft_moves_list_size(head) != 0)
		ft_moves_list_del(&head);
	ft_print_co_ord(co_ord[0] - player->offset[0], co_ord[1] - player->offset[3]);
}
