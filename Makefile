# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/10 10:39:48 by tstephen          #+#    #+#              #
#    Updated: 2018/07/14 14:09:27 by tstephen         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = a.out

$(NAME): fclean
	@make re -C libft/ && make clean -C libft/
	@gcc -Wall -Wextra -Werror -g \
	-lmlx -framework OpenGL -framework AppKit \
	libft/libft.a \
	main.c \
	map.c \
	token.c \
	utility.c \
	valid_moves.c \
	distance.c \
	graphics.c \
	play.c

all: $(NAME)

clean:
	@/bin/rm -rf *.o

fclean: clean
	@/bin/rm -rf $(NAME)* 

re: fclean all
