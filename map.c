/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 12:36:23 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/16 10:27:03 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_get_map_dimensions(t_map *map, char *line)
{
	map->row_map = ft_atoi(line + 8);
	map->col_map = ft_atoi(line + 9 + ft_nbrlen(map->row_map));
}

void		ft_get_map(t_map *map, int fd)
{
	int		row;
	char	*line;

	row = 0;
	get_next_line(fd, &line);
	free(line);
	map->map = (char**)ft_memalloc(sizeof(char*) * map->row_map);
	while (row < map->row_map)
	{
		get_next_line(fd, &line);
		(map->map)[row] = ft_strsub(line, 4, map->col_map);
		free(line);
		row++;
	}
}
